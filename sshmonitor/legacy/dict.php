<?php
/*
 * TODO: check license
 * dict.php -  NfSen plugin for massive brute force attack detection 
 *
 * Copyright (C) 2010, 2011 Masaryk University
 * Author: Jan VYKOPAL <vykopal@ics.muni.cz>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name of the Masaryk University nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * This software is provided ``as is'', and any express or implied
 * warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed.
 * In no event shall the Masaryk University or contributors be liable for
 * any direct, indirect, incidental, special, exemplary, or consequential
 * damages (including, but not limited to, procurement of substitute
 * goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether
 * in contract, strict liability, or tort (including negligence or
 * otherwise) arising in any way out of the use of this software, even
 * if advised of the possibility of such damage.
 *
 * $Id: dict.php 2152 2013-08-05 13:15:39Z 207426 $
 *
 */

/*
 * Frontend plugin: dict
 *
 * Required functions: dict_ParseInput and dict_Run
 *
 */

/* 
 * dict_ParseInput is called prior to any output to the web browser 
 * and is intended for the plugin to parse possible form data. This 
 * function is called only, if this plugin is selected in the plugins tab. 
 * If required, this function may set any number of messages as a result 
 * of the argument parsing.
 * The return value is ignored.
 */
function dict_ParseInput( $plugin_id ) {

	#SetMessage('error', "Chyba!");
	#SetMessage('warning', "Varovani!");
	#SetMessage('alert', "Upozorneni!");
	#SetMessage('info', "Info!");

} // End of demoplugin_ParseInput


/*
 * This function is called after the header and the navigation bar have 
 * are sent to the browser. It's now up to this function what to display.
 * This function is called only, if this plugin is selected in the plugins tab
 * Its return value is ignored.
 */
function dict_Run( $plugin_id ) {

	// the command to be executed in the backend plugin
	$command = 'dict::getStats';

	// two scalar values
	$colour1 = '#72e3fa';
	$colour2 = '#2a6f99';

	// one array
	$colours = array ( '#12af7d', '#56fc7b');

	// prepare arguments
	$opts = array();
	$opts['colour1'] = $colour1;
	$opts['colour2'] = $colour2;
	$opts['colours'] = $colours;

	// call command in backened plugin
        $out_list = nfsend_query($command, $opts);

	// get result
        if ( !is_array($out_list) ) {
                SetMessage('error', "Error calling backend plugin");
                return FALSE;
        }

        $count_5mins = $out_list['count-5mins'];
        $count_1hour = $out_list['count-hour'];
        $count_1day = $out_list['count-day'];
        
        # $attackers = $out_list['attackers'];
        print "# Recent SSH scans: last 5 minutes: <strong>$count_5mins</strong>, last hour: <strong>$count_1hour</strong>, last day: <strong>$count_1day</strong><br/><br/>\n";
        # print $attackers;
#        kod nize daval: Warning: Invalid argument supplied for foreach()        
#        foreach ($attackers as $row) {
#                        print $row.'<br/>';
#        }

} // End of dict_Run


?>
