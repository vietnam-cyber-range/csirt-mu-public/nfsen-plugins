+----------------------------------+
| README - NfSen SSHMonitor Plugin |
+----------------------------------+

Authors           Jan Vykopal, vykopal@ics.muni.cz
                  Martin Vizvary, vizvary@ics.muni.cz
Version           1.0.0 RC
Release date      2013-05-21

SSHMonitor - SSH service monitoring plugin for NfSen provides
information about SSH activity in network based on NetFlow data. 
SSHMonitor detects brute force and dictionary attacks on host with open TCP
port 22. The plugin reports attackers that threatens local network. Web
interface present captured flows and statistics about recent network activity.

Content:
========
1. License
2. Requirements
3. Installation
4. Configuration
  4.1 PostgreSQL database settings
  4.2 Creating NfSen profile
  4.3 Plugin configuration
  4.4 Editing e-mail messages
  4.5 Reload of NfSen
5. Using the SSHMonitor plugin interface
6. Uninstallation
  6.1 Uninstallation of SSHMonitor plugin
  6.2 Uninstallation of database


1. License:
===========
  * The plugin frontend includes the third party software component JQuery,
    distributed under the MIT License, Highcharts under Creative Commons 
    Attribution-NonCommercial 3.0 License.
  * The plugin backend uses the third party software component PostgreSQL database.
  * The plugin backend and frontend code is available under the BSD License.


2. Requirements:
================
  * Perl 5.8.8 or higher
  * PHP 5.5.3 or higher
  * PostgreSQL 8.4.9 or higher or SQLite 3.3.6 or higher
  * NfSen 1.3.6 or higher
  * nfdump 1.6.5 or higher
  * Perl-dependencies:
    * DBD::Pg or DBD::SQLite (depending of the used database)
    * DBI
    * DateTime
    * Email::Simple
    * IP::Country::Fast
    * Net::CIDR
    * POSIX
    * Sys::Syslog
    * Socket
    * URI::Escape

3. Installation:
================

1. Unpack SSHMonitor tarball.

    $ tar xzvf sshmonitor-0.9.0.tar.gz

Copy the content of backend directory to $BACKEND_PLUGINDIR
the content of frontend directory to $FRONTEND_PLUGINDIR
 
    $ cp -R ./backend/* $BACKEND_PLUGINDIR 
    $ cp -R ./frontend/* $FRONTEND_PLUGINDIR

$BACKEND_PLUGINDIR and $FRONTEND_PLUGINDIR are specified in /data/nfsen/etc/nfsen.conf.

Make sure that SSHMonitor will be able to create and write to files in $BACKEND_PLUGINDIR/SSHMonitor/


4. Configuration:
=================

4.1 Database settings
Plugin supports PostgreSQL and SQLite databases. For the long run, we recommend using PostgreSQL database due
to SQLite performance.


4.1.1 PostgreSQL database settings
----------------------------------
Assuming the PostgreSQL database is installed and running
log in as database admin

    $ su -
    # su - postgres
    $ psql

Create the database

    =# CREATE DATABASE sshmonitordb;

Create new database user with <password>

    =# CREATE USER sshmonitor WITH PASSWORD '<password>';

Grant all privileges on SSHMonitor database to this user

    =# GRANT ALL ON DATABASE sshmonitordb TO sshmonitor;

Exit PostgreSQL console

    =# \q
    
Create tables
  
    $ psql -U sshmonitor -d sshmonitordb < ./backedn/PostgreSQL-init

Tables and indexes will be created at first initialization.

4.1.2 SQLite database settings
------------------------------
The package contains SQLite file with database schema in SSHMonitor folder (SSHMonitor-1.0.0/backend/SSHMonitor/sshmonitoring). User running nfsen must have granted RW access to the database file. We recommend to use SQLite only for testing for several weeks. When the database file grows to the several hundreds MBs, the frontend is unstable.

4.2 Creating NfSen profile
--------------------------
Plugin needs specific profile for SSH traffic. Name of the profile have to be the same
as in nfsen.conf specified in 4.4. A guide to the creating of profiles could be found at NfSen homepage - http://nfsen.sourceforge.net/#mozTocId623518

Profile must contain three channels (channels can be from the same source/interface):
  * <channel in> - incoming SSH related packets, filter: proto tcp and port 22 and dst net <local network address>  
  * <channel out> - outgoing SSH related packets, filter: proto tcp and port 22 and src net <local network address>
  * <channel in-scan> - incoming scan SSH packets, filter: port 22 and proto tcp and dst net <local network address>
                      and flags S and not flags ARPUF

4.3 Plugin Configuration
------------------------
Add the following line to the @plugins section of nfsen.conf

  [ '<profile-name>', 'SSHMonitor' ],

Modify %PluginConf hash variable in nfsen.conf:

Plugin configuration (using PostgreSQL):

%PluginConf = (
  ...
  SSHMonitor => {
    sqlite           => 0,
    db_name          => '<dbname>',
    db_host          => '<dbhost>',
    db_port          => '<dbport>',
    db_user          => '<dbuser>',
    db_passwd        => '<dbpasswd>',
    mail_to          => 'operator@your.domain',
    mail_from        => 'SSHMonitor@your.domain',
    channel_in       => '<channel in>',
    channel_out      => '<channel out>',
    channel_scan     => '<channel in-scan>',
    local_network    => '<local network address>',
    whitelist        => '<whitelist>',
  },
  ...
);

Plugin configuration (using SQLite):

%PluginConf = (
  ...
  SSHMonitor => {
    sqlite           => 1,
    sqlite_path      => '<path to SQLite database file>'
    mail_to          => 'operator@your.domain',
    mail_from        => 'SSHMonitor@your.domain',
    channel_in       => '<channel in>',
    channel_out      => '<channel out>',
    channel_scan     => '<channel in-scan>',
    local_network    => '<local network address>',
    whitelist        => '<whitelist>',
  },
  ...
);

- <db_name>         name of the database specified in 4.1 ("sshmonitordb")
- <db_host>         hostname of remote database host or "localhost" if the database runs locally
- <db_port>         database port (usually 5432)
- <db_user>         database user created in 4.1 ("sshmonitor")
- <db_passwd>       password for the database user created in 4.1 ("password")
- <mail_to>         e-mail address that will receive all the notifications
- <mail_from>       sender's address
- <channel_in>      NfSen channel for incoming SSH communication in local network
- <channel_out>     NfSen channel for outgoinh SSH communication from local network
- <channel_scan>    NfSen channel for incoming SSH scan communication in local network
- <local_network>   local network range in CIDR format
- <whitelist>       comma separated list of networks (full net, not e. g. 192.168/16)


4.4 Editing e-mail messages
---------------------------
You can edit e-mail message from Frontend part of plugin. Email template (email.txt) includes short description of exported 
variables you can use in your text (e.g., reported IP, current time, etc.).


4.5 Reload of NfSen
-------------------
    
    $BINDIR/nfsen reload


5. Using SSHMonitor plugin interface:
==========================

SSHMonitor interface is accesible via NfSen web interface in the 'Plugins' section.
First you see 'Overview' tab containing graphs of most active suspicious addresses, victims,
attacks and scans statistics. You can see trends from which country the most attackers
come from. Tabs 'Attacks' and 'Scans' are supposed to present data for network analysis
from captured flows. 'Settings' tab allows user to change e-mail content and revise
settings in nfsen.conf.

6. Uninstallation:
==================

6.1 Uninstallation of SSHMonitor plugin
---------------------------------------
Stop NfSen
    
    $BINDIR/nfsen stop

Edit file nfsen.conf - delete line

  [ 'ssh', 'SSHMonitor' ],

in @plugins section.
Delete hash variable entry

  SSHMonitor => {
    ...
  }

in section %PluginConf.

Remove files and directories

    $ rm -rf $BACKEND_PLUGINDIR/SSHMonitor*
    $ rm -rf $FRONTEND_PLUGINDIR/SSHMonitor*

6.2 Uninstallation of PostgreSQL database
-----------------------------------------
Connect to database as admin, delete SSHMonitor database in PostgreSQL console:

    $ su -
    # su - postgres
    $ psql

    =# DROP DATABASE sshmonitordb
    =# \q
