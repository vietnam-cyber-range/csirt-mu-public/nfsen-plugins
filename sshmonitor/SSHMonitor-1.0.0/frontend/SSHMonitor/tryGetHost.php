<?php
#
# tryGetHost.php -  prints out hostname for given ip
#
# Copyright (C) 2012 Masaryk University
# Authors: Jan Vykopal <vykopal@ics.muni.cz>
#          Martin Vizvary, <vizvary@ics.muni.cz>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the Masaryk University nor the names of its
#    contributors may be used to endorse or promote products derived from
#     this software without specific prior written permission.
#
# This software is provided ``as is'', and any express or implied
# warranties, including, but not limited to, the implied warranties of
# merchantability and fitness for a particular purpose are disclaimed.
# In no event shall the Masaryk University or contributors be liable for
# any direct, indirect, incidental, special, exemplary, or consequential
# damages (including, but not limited to, procurement of substitute
# goods or services; loss of use, data, or profits; or business
# interruption) however caused and on any theory of liability, whether
# in contract, strict liability, or tort (including negligence or
# otherwise) arising in any way out of the use of this software, even
# if advised of the possibility of such damage.
#
# Version: 0.9.0
#

/*
 * tryGetHost function find hostname.
*/

function tryGetHost($ip)
    {
        $string = '';
        exec("dig +short -x $ip 2>&1", $output, $retval);
        if ($retval != 0)
        {
            // there was an error performing the command
        }
        else
        {
            $x=0;
            while ($x < (sizeof($output)))
            {
                $string.= $output[$x];
                $x++;
            }
        }

        if (empty($string))
            $string = "-";
        else //remove the trailing dot
            $string = substr($string, 0, -1);

        return $string;
    }
?>
