<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link href="plugins/RdpMonitor/css/jquery-ui.css" rel="stylesheet" type="text/css">
  <link href="plugins/RdpMonitor/css/RdpMonitor.css" rel="stylesheet" type="text/css">

  <title></title>
  </head>
  <body style="font-family:Verdana; font-size:10px;" onclick="window.close();">

  <?php
    $ip = $_GET['ip'];
    $dbconn = pg_connect("host=datos-devel.ics.muni.cz dbname=test user=test password=0b3d4s1t3st") or die('Unable to connect to DB: ' . pg_last_error());

    $result = pg_query("SELECT to_char(ts, 'DD. MM. YYYY HH24:MI:SS') as ts,dip,flows FROM attacks WHERE sip = '$ip' AND date(ts) >= current_date-7 ORDER BY ts");
    
    print "<h2> Detail komunikace IP adresy $ip</h2>";
    print '<table class="data_table" cellpadding="0" cellspacing="0" border="1">';
    print '<tr style="background-color: #cedfda;">
             <th width=250>Timestamp</th>
             <th width=170>Destination</th>
             <th width=100># of flows</th>
          </tr>';


    while($row = pg_fetch_assoc($result)){
      print "<tr>
              <td align=center>" . $row['ts'] . "</td>
              <td >" . $row['dip'] . "</td>
              <td align=center>" . $row['flows'] . "</td>
            </tr>";
    }
    print '</table>';

    pg_free_result($result);
  
    pg_close($dbconn) or die('Unable to close connection: ' . pg_last_error()) ;

  ?>
  </body>
</html>
