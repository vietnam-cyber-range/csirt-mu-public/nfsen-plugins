<?php 
/*
 *RdpMonitor_get_highchart_ip_details function is called from the RdpMonitor_Run function.
 * It takes one argument - array otps with options for backend function. It must
 * contain name of the graph (under graph_name key) and unix timestamps of begin
 * and end of the displayed data (under keys begin and end).
 *
 * The function will request data from backend and returns JavaScript code that
 * will draw the graph with Highcharts library.
 */

function RdpMonitor_get_highchart_ip_details($opts) {

	// Request the data for the graph from backend function feedGraph
        $out_list = nfsend_query("RdpMonitor::feed_graph", $opts);
        if ( !is_array($out_list) ) {
                print "Error calling backend plugin - feed_graph\n $out_list\n 1";return FALSE;
        }

        $data = $out_list["ipDetailsMonthly"]; 
        $categories = '[';
        $series = array();
        for($i =0; $i<count($data); $i=$i+3){
          $date = $data[$i];
          if($data[$i+1] == ''){
            $country = "--";
          } else {
            $country = $data[$i+1];
          }
          $count = $data[$i+2];
 
          if(!strstr($categories, $date)){
            $categories .= "'" . $date . "',";
          }

          $counter = substr_count($categories, ',');
          $gap = $counter - substr_count($series[$country], ',');
         
          if(array_key_exists($country, $series)){
              for($j = 1; $j < $gap; $j++){
                $series[$country] .= "null,";
              }
              $series[$country] .= $count . ",";
          } else {
              $series[$country] = "[";
              for($j = 1; $j < $gap; $j++){
                $series[$country] .= "null,";
              } 
              $series[$country] .= $count . ",";   
          }
        }

        $js_code = "var cat = " . $categories . "];";
        $js_code .= "var series = ["; 

        foreach($series as $key => $value){
          $country = country_code_to_country($key);
          $js_code .= "{name: '" . $country ." (".$key . ")', data: " . $value . "]},";
        }
        $js_code .= "];";
	// Generate JavaScript arrays definitions of graph lines
        $js_code .= "
  var chart3;
  chart3 = new Highcharts.Chart({
    chart: {
             renderTo: 'highchart-ip_details',
             type: 'column',
             width: 1465,
	     borderWidth: 1,
	     plotBorderWidth: 1
           },
    colors: color2,
    title: {
            text: 'Top 7 countries over the last months'
           },
    tooltip: {
               useHTML: true,
               shadow: false,
               pointFormat: '{series.name}: <b>{point.y}</b><br/> ',
               percentageDecimals: 1
             },

    xAxis: {
             categories: cat 
           },
    yAxis: {
             title: {
                     text: 'Total unique attackers'
                    },
             type: 'linear',
           },
    legend: {
             align: 'right',
             verticalAlign: 'top',
             y: 40, 
             floating: true,
             backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
             borderColor: '#CCC',
             labelFormatter: function() {
               return this.name.substr(-4);
             },
             borderWidth: 1,
             shadow: false
            },
    series: series
     });
";
/*
    $data = $out_list["ipDetails"]; 
    $js_code .= "data1 = [";
    for($i =0; $i<count($data); $i=$i+2){
          $js_code .= "['". $data[$i] ."', ". $data[$i+1] ."],"; 
        }
        $js_code.= "];";
	// Generate JavaScript arrays definitions of graph lines
        $js_code .= "

    var chart4;
    $(document).ready(function() {
        // Build the chart
        chart4 = new Highcharts.Chart({
            chart: {
                renderTo: 'highchart-ip_details2',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            colors: color2,
            title: {
                text: 'Browser market shares at a specific website, 2010'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b><br/>',
                percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.y;
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data: data1                
            }]
        });
    });
  ";
*/
	return $js_code;
}
?>
