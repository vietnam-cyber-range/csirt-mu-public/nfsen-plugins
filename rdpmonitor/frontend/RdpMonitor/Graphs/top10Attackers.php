<?php 
  
  /*
   *RdpMonitor_get_highchart_top10_attackers function is called from the RdpMonitor_Run function.
   * It takes one argument - array otps with options for backend function. It must
   * contain name of the graph (under graph_name key) and unix timestamps of begin
   * and end of the displayed data (under keys begin and end).
   *
   * The function will request data from backend and returns JavaScript code that
   * will draw the graph with Highcharts library.
   */
  function RdpMonitor_get_highchart_top10_attackers($opts) {
  
  	// Request the data for the graph from backend function feedGraph
          $out_list = nfsend_query("RdpMonitor::feed_graph", $opts);
          if ( !is_array($out_list) ) {
                  print "Error calling backend plugin - feed_graph\n $out_list\n 1";return FALSE;
          }
	  $numerator = 60 * 5; // 60 seconds per minute * 5 minutes
	  $rounded_time = ( round ( time() / $numerator ) * $numerator ); // Calculate time to nearest 5 minutes!

          $data = ($out_list["top10_attackers"]);
          $ips = '';
 // url: 'plugins/RdpMonitor/attackerIpDetails.php?ip=" . $data[$i] . "'},";
          for($i =0; $i < count($data); $i=$i+4){
            $cc = $data[$i+3];
            $dataPie[$cc] = $dataPie[$cc]+1;
            $ips .= "'".$data[$i]. "',";
            $addData .= "{ dnsname: '" . $data[$i+2] . "',
                         country: '" . $data[$i+3]. "',
                         y: " . $data[$i+1] . ",
			 ip: '".$data[$i]."'},";
  
          }
          $chartDataPie = '';
          foreach($dataPie as $country => $count){
            $chartDataPie .= "{name: '" . $country . "', y: " . $count ."},"; 
          }
          $js_code .= "
          var chart = new Highcharts.Chart({
            chart: 
            {
              renderTo: 'highchart-top10_attackers',
              type: 'column',
              width: '700',
              borderWidth: 1,
              plotBorderWidth: 1
            },
            title:
            {
              text: 'Top 10 attackers',
              style: { color: 'black' } 
            },
            subtitle: {
              text: 'Floating 7 days window'
            },
            legend: 
            {
              enabled: false
            },
            plotOptions:
            {
              series:
              {
                cursor: 'pointer',
                point: 
                {
                  events: 
                  {
                    click: function(){
			     $('#begin_datetime_attacks').val('".date('Y-m-d H:i', $rounded_time -604800)."');
			     $('#end_datetime_attacks').val('".date('Y-m-d H:i', $rounded_time)."');
			     $('#src_ip_attacks').val(this.options.ip);
			     $('#timewindow_attacks').val('1 week');
			     $('#tab-container').easytabs('select', '#tab_attacks');
			     $('#time_form_attacks').submit();
                           }
                  }
                }
              }
            },
            xAxis:
            {
              labels: 
              { 
                rotation: -80,
                y: 55  
              },
              title: 
              {
                text: 'IP address of attacker'
              },
            
              categories: [$ips],
              formatter:
                function() {
                  return this.value;
                }
            },
            yAxis:
            { 
              title: {
		text: 'Quantity'
	      }
            },
            tooltip: {
              useHTML: true,
              shadow: false,
              formatter: function() {
                return '<b>IP: </b>'+ this.x + ' ('+this.point.dnsname +')' +
                       '<br /><b>Country: </b>' + this.point.country +
                       '<br /><b>Number of unique victims: </b>'+ this.y;
              }   
            },
            series: [{data: [$addData]}]
      });
   ";
  
  	return $js_code;
  }
?>
